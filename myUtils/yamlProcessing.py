# -*- coding: windows-1251 -*-

import yaml  # pip install PyYAML

# ������ ��� ������ ������ � yaml �����.
# �������� �������� ������, ���� �� �����.
# ����� ����������� ��������� �������� ��� �������� ������
key_path = []


def find(d, tag):
    """
    ����������� ����� ��������� ����� � YAML �����
    �������� ���� ���������� � ���������� ���������� keyPath
    :param tag - ������� ����
    :param d - yaml ���� � ������� ���� �����"""
    if tag in d:
        key_path.append(tag)
        yield d[tag]
    for k, v in d.items():
        if isinstance(v, dict):
            for i in find(v, tag):
                key_path.append(k)
                yield i


def openYaml(path, mode):
    """�������� ��������� YAML �����"""
    stream = open(path, mode)
    result = yaml.safe_load(stream)
    return result


def prepareCommand(target_file):
    """
    ������������ ����� ������� ��� ����������. �������� �� ����(������ ���������� � keyPath � ���������
    ������ ������� � �������� ���������.
    :param target_file: ���� ��� ������� ������������ ��������
    :return: - ���������� ��������� ������� � �����
    """
    command = f"{target_file}"
    for item in key_path:
        command += f'["{item}"]'
    return command


def main(target_file, target_file_mode, required_key):
    """
    ��������� YAML ����, ������� � ��� �������� ����, ��������� ���� � ���� � ������, ���������
    �������� �������� ��� ��������� ������.
    :param target_file: - YAML ���� ������� ����� ���������
    :param target_file_mode: - ������ ������� � ����� (������/������/����������)
    :param required_key: - ��� �������� ����� � �����
    :return:
    """
    global key_path
    data = openYaml(target_file, target_file_mode)
    for val in find(data, required_key):
        key_path = key_path[::-1]  # ������������� ��������� ���� �� �����.
        text = str(prepareCommand(data))  # ������� �������� �� ����������
        value = str(eval(text))  # ��������� ��������� ��������
        yield key_path, value
        key_path = []

# if __name__ == '__main__':
#     main()
